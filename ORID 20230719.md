# ORID 2023/07/19

## O

- Integration Automation Testing. 
- Test Pyramid



## R

- More relieved.



## I

- Today I learned about Integration Automation Testing, which protects functionality from being impacted by refactoring by writing corresponding tests to interfaces.
- . In a system often integrate many services, under each service there are many interfaces, the interface contains several functions. We should use unit tests to ensure the usability of individual functions, service tests to ensure the normal combination of functions within the service and the realization of the function, and UI tests to ensure the usability of the system. A large number of unit tests are the cornerstone of a stable and usable system, and crosstesting ensures overall usability.




## D

- Assertion api for testing is unfamiliar



- Learning the very important method of being able to test the implementation of current functionality while assuming that the predecessor functionality is working properly is very helpful in teamwork.

